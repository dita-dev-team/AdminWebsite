import time
import unittest

import bcrypt
import requests
from flask.ext.testing import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.wait import WebDriverWait
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from api.models import Admin, Member, Laptop, Log
from api import app, db


class MyNewTestCase(LiveServerTestCase):
    BASE_URL = 'http://localhost:8112'
    Session = None

    @classmethod
    def setUpClass(cls):
        cls.engine = create_engine('postgresql://pgorzjncgvddpf:7310fe26d10baf1ad358b1289286c100167196552e806583726bca809e951bbb@ec2-54-163-254-48.compute-1.amazonaws.com:5432/deubs8ovl36su2')
        cls.Session = sessionmaker(bind=cls.engine)
        #db.drop_all()

    def setUp(self):
        db.create_all()
        self.driver = webdriver.PhantomJS()
        self.driver.set_window_size(800, 600)

    def tearDown(self):
        self.driver.close()
        db.drop_all()

    def create_app(self):
        app.config['TESTING'] = True
        app.config['LIVESERVER_PORT'] = 8112
        app.config['LIVESERVER_TIMEOUT'] = 10
        return app

    def test_server_is_up_and_running(self):
        response = requests.get(self.BASE_URL)
        assert response.status_code == 200

    #@unittest.skip('skipping test_redirect')
    def test_redirect(self):
        self.driver.get(self.BASE_URL)
        assert 'username' in self.driver.page_source

        link = self.driver.find_element_by_link_text('Members')
        link.click()

        assert 'username' in self.driver.page_source

        link = self.driver.find_element_by_link_text('Laptops')
        link.click()

        assert 'username' in self.driver.page_source

    #@unittest.skip('skipping test_login')
    def test_login(self):
        self.create_admin()
        self.driver.get(self.BASE_URL)
        self.login()

        assert 'Welcome to the Dita Admin Site' in self.driver.page_source

    #@unittest.skip('skipping test_members')
    def test_members(self):
        self.create_admin()
        self.driver.get(self.BASE_URL + "/members")
        self.login()

        button = self.driver.find_element_by_id('create-btn')
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        id_no = '14-2873'
        name = 'michael'
        name2 = 'michael78'

        element = self.driver.find_element_by_id('id_no')
        element.send_keys(id_no)

        element2 = self.driver.find_element_by_id('name')
        element2.send_keys(name)

        select = Select(self.driver.find_element_by_id('major'))
        select.select_by_index(0)

        button = self.driver.find_element_by_id('create')
        button.click()

        assert 'Member created successfully.' in self.driver.page_source
        session = self.Session()
        member = session.query(Member).get(id_no)


        assert id_no == member.id_no
        assert name == member.name
        session.close()

        button = self.driver.find_element_by_id('create-btn')
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element = self.driver.find_element_by_id('id_no')
        element.send_keys(id_no)

        element2 = self.driver.find_element_by_id('name')
        element2.send_keys(name)

        select = Select(self.driver.find_element_by_id('major'))
        select.select_by_index(0)

        button = self.driver.find_element_by_id('create')
        button.click()

        assert 'Member already exists' in self.driver.page_source

        time.sleep(0.5)

        button = self.driver.find_element_by_id("close-btn")
        button.click()

        time.sleep(0.5)

        button = self.driver.find_element_by_name(id_no)
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element = WebDriverWait(self.driver, 5).until(
            expected_conditions.text_to_be_present_in_element_value((By.NAME, 'id_no'), '14-2873')
        )

        element = self.driver.find_element_by_id('id_no')

        assert not element.is_enabled()

        element2 = self.driver.find_element_by_id('name')
        element2.clear()
        element2.send_keys(name2)

        select = Select(self.driver.find_element_by_id('major'))
        select.select_by_index(1)

        button = self.driver.find_element_by_id('update')
        button.click()

        assert 'Member updated successfully.' in self.driver.page_source

        session = self.Session()
        member = session.query(Member).get(id_no)

        assert 'MIS' == member.major
        assert name2 == member.name
        session.close()

        button = self.driver.find_element_by_name(id_no)
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element = WebDriverWait(self.driver, 5).until(
            expected_conditions.text_to_be_present_in_element_value((By.NAME, 'id_no'), '14-2873')
        )

        button = self.driver.find_element_by_id('delete')
        button.click()

        assert 'Member deleted successfully.' in self.driver.page_source

        session = self.Session()
        members = session.query(Member).all()

        assert len(members) == 0
        session.close()

    #@unittest.skip('skipping test_laptops')
    def test_laptops(self):
        self.create_admin()
        self.driver.get(self.BASE_URL + "/members")
        self.login()

        button = self.driver.find_element_by_id('create-btn')
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        id_no = '14-2873'
        name = 'michael'

        element = self.driver.find_element_by_id('id_no')
        element.send_keys(id_no)

        element2 = self.driver.find_element_by_id('name')
        element2.send_keys(name)

        select = Select(self.driver.find_element_by_id('major'))
        select.select_by_index(0)

        button = self.driver.find_element_by_id('create')
        button.click()

        link = self.driver.find_element_by_link_text('Laptops')
        link.click()

        button = self.driver.find_element_by_id('create-btn')
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        serial_no = 'XXXXXX'
        make = 'DELL'
        make2 = 'HP'

        element = self.driver.find_element_by_id('serial_no')
        element.send_keys(serial_no)

        element2 = self.driver.find_element_by_id('make')
        element2.send_keys(make)

        element2 = self.driver.find_element_by_id('id_no')
        element2.send_keys('00-0000')

        button = self.driver.find_element_by_id('create')
        button.click()

        assert 'Member does not exist.' in self.driver.page_source

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element2 = self.driver.find_element_by_id('id_no')
        element2.clear()
        element2.send_keys(id_no)

        button = self.driver.find_element_by_id('create')
        button.click()

        assert 'Laptop created successfully.' in self.driver.page_source

        session = self.Session()
        laptop = session.query(Laptop).filter(Laptop.serial_no.ilike(serial_no)).first()
        session.close()

        assert serial_no == laptop.serial_no
        assert make == laptop.make
        assert id_no == laptop.owner.id_no

        button = self.driver.find_element_by_id('create-btn')
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element = self.driver.find_element_by_id('serial_no')
        element.send_keys(serial_no)

        element2 = self.driver.find_element_by_id('make')
        element2.send_keys(make)

        element2 = self.driver.find_element_by_id('id_no')
        element2.send_keys(id_no)

        button = self.driver.find_element_by_id('create')
        button.click()

        assert 'Laptop already registered.' in self.driver.page_source

        time.sleep(0.5)

        button = self.driver.find_element_by_id("close-btn")
        button.click()

        time.sleep(0.5)

        button = self.driver.find_element_by_name(serial_no)
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element = WebDriverWait(self.driver, 5).until(
            expected_conditions.text_to_be_present_in_element_value((By.NAME, 'serial_no'), serial_no)
        )

        element = self.driver.find_element_by_id('serial_no')

        assert not element.is_enabled()

        element2 = self.driver.find_element_by_id('make')
        element2.clear()
        element2.send_keys(make2)

        button = self.driver.find_element_by_id('update')
        button.click()

        assert 'Laptop updated successfully.' in self.driver.page_source

        session = self.Session()
        laptop = session.query(Laptop).filter(Laptop.serial_no.ilike(serial_no)).first()
        session.close()

        assert make2 == laptop.make

        button = self.driver.find_element_by_name(serial_no)
        button.click()

        self.driver.switch_to.active_element

        time.sleep(0.5)

        element = WebDriverWait(self.driver, 5).until(
            expected_conditions.text_to_be_present_in_element_value((By.NAME, 'serial_no'), serial_no)
        )

        button = self.driver.find_element_by_id('delete')
        button.click()

        assert 'Laptop deleted successfully.' in self.driver.page_source

        session = self.Session()
        laptops = session.query(Laptop).all()

        assert len(laptops) == 0

        members = session.query(Member).all()

        assert len(members) > 0

        session.close()

    # @unittest.skip('skipping test_logout')
    def test_logout(self):
        self.create_admin()
        self.driver.get(self.BASE_URL)
        self.login()

        link = self.driver.find_element_by_link_text('Logout')
        link.click()

        assert 'username' in self.driver.page_source

    def create_admin(self):
        session = self.Session()
        password = bcrypt.hashpw('admin'.encode('utf-8'), bcrypt.gensalt(10)).decode('utf-8')
        admin = Admin(username='admin', password=password)
        session.add(admin)
        session.commit()

    def login(self):
        element = self.driver.find_element_by_id('username')
        element.send_keys('admin')

        element2 = self.driver.find_element_by_id('password')
        element2.send_keys('admin')
        element2.submit()


if __name__ == '__main__':
    unittest.main()
