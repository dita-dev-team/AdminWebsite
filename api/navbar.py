from flask.ext.nav.elements import Navbar, View


def login_nav():
    return Navbar(
        "Dita Admin",
        View('Home', 'index'),
        View('Members', 'members'),
        View('Laptops', 'laptops'),
        View('Login', 'login')
    )


def logout_nav():
    return Navbar(
        "Dita Admin",
        View('Home', 'index'),
        View('Members', 'members'),
        View('Laptops', 'laptops'),
        View('Logout', 'logout')
    )
