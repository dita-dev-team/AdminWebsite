import warnings


from flask.exthook import ExtDeprecationWarning

warnings.simplefilter('ignore', ExtDeprecationWarning)
from flask import Flask
from flask_wtf import CsrfProtect
from flask_bcrypt import Bcrypt
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_nav import Nav
from api import config
from api.navbar import login_nav

app = Flask(__name__)
app.debug = True
app.config.from_object(config)
CsrfProtect(app)
bcrypt = Bcrypt(app)
Bootstrap(app)

nav = Nav()
nav.register_element('top_navbar', login_nav)

nav.init_app(app)

db = SQLAlchemy(app)
# db.create_all()

import api.views
