import datetime


def get_current_time():
    return datetime.datetime.now()


def get_current_date():
    return datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
