from flask import jsonify, redirect, request, render_template, url_for
from flask_login import current_user, login_required, login_user, logout_user, LoginManager

from api import app, bcrypt, nav, db
from api.forms import LoginForm, MemberForm, LaptopForm
from api.models import Admin, Member, Laptop
from api.navbar import logout_nav, login_nav

login_manager = LoginManager()
login_manager.login_view = '/login'
login_manager.init_app(app)


@app.route('/login', methods=['GET', 'POST'])
def login():
    nav.register_element('top_navbar', login_nav)
    form = LoginForm(request.form)
    if form.validate_on_submit():
        admin = Admin.query.filter(Admin.username.ilike(form.username.data)).first()
        if admin:
            if bcrypt.check_password_hash(admin.password, form.password.data):
                admin.authenticated = True
                login_user(admin, remember=True)
                #nav.register_element('top_navbar', logout_nav)
                return form.redirect(url_for('index'))

    if form.errors:
        form.errors_list = list(form.errors.values())

    return render_template('login.html', form=form)


@app.route('/logout', methods=['GET'])
def logout():
    admin = current_user
    admin.authenticated = False
    logout_user()
    nav.register_element('top_navbar', login_nav)
    return redirect(url_for('index'))


@app.route('/', methods=['GET', 'POST'])
@login_required
def index():
    nav.register_element('top_navbar', logout_nav)
    return render_template('index.html')


@app.route('/members', methods=['GET', 'POST'])
@login_required
def members():
    nav.register_element('top_navbar', logout_nav)
    notification = ""
    if 'notification' in request.args:
        notification = request.args['notification']

    form = MemberForm(request.form)
    form.crud_operation = request.form['submit'] if 'submit' in request.form else None
    form.update_key = request.form['update-key'] if 'update-key' in request.form else None
    if form.validate_on_submit():
        if request.form['submit'] == 'create':
            member = Member(id_no=request.form['id_no'], name=request.form['name'], major=request.form['major'])
            db.session.add(member)
            db.session.commit()
            notification = "Member created successfully."
        elif request.form['submit'] == 'delete':
            member = db.session.query(Member).get(request.form['id_no'])
            db.session.delete(member)
            db.session.commit()
            notification = "Member deleted successfully."
        elif request.form['submit'] == 'update':
            member = db.session.query(Member).get(request.form['id_no'])
            member.name = request.form['name']
            member.major = request.form['major']
            db.session.commit()
            notification = "Member updated successfully."

        return redirect(url_for('members', notification=notification))

    if form.errors:
        form.errors_list = list(form.errors.values())

    all_members = Member.query.all()
    return render_template('members.html', form=form, members=all_members, notification=notification)


@app.route('/member/<id_no>')
def member(id_no):
    member = Member.query.get(id_no)
    response = jsonify({})

    if member:
        response = jsonify({
            'id_no': member.id_no,
            'name': member.name,
            'major': member.major
        })

    return response


@app.route('/laptops', methods=['GET', 'POST'])
@login_required
def laptops():
    nav.register_element('top_navbar', logout_nav)
    notification = ""
    if 'notification' in request.args:
        notification = request.args['notification']

    form = LaptopForm(request.form)
    form.crud_operation = request.form['submit'] if 'submit' in request.form else None
    form.update_key = request.form['update-key'] if 'update-key' in request.form else None
    if form.validate_on_submit():
        if request.form['submit'] == 'create':
            member = Member.query.get(request.form['id_no'])
            laptop = Laptop(serial=request.form['serial_no'], make=request.form['make'], owner=member)
            db.session.add(laptop)
            db.session.commit()
            notification = "Laptop created successfully."
        elif request.form['submit'] == 'delete':
            laptop = Laptop.query.filter(Laptop.serial_no.ilike(request.form['serial_no'])).first()
            db.session.delete(laptop)
            db.session.commit()
            notification = "Laptop deleted successfully."
        elif request.form['submit'] == 'update':
            laptop = Laptop.query.filter(Laptop.serial_no.ilike(request.form['serial_no'])).first()
            laptop.make = request.form['make']
            laptop.owner = Member.query.get(request.form['id_no'])
            db.session.commit()
            notification = "Laptop updated successfully."

        return redirect(url_for('laptops', notification=notification))
    if form.errors:
        form.errors_list = list(form.errors.values())

    all_laptops = Laptop.query.all()
    return render_template('laptops.html', form=form, laptops=all_laptops, notification=notification)


@app.route('/laptop/<serial>')
def laptop(serial):
    laptop = Laptop.query.filter(Laptop.serial_no.ilike(serial)).first()
    response = jsonify({})

    if member:
        response = jsonify({
            'serial_no': laptop.serial_no,
            'make': laptop.make,
            'owner': laptop.owner.id_no
        })

    return response


@app.route('/api/members')
def get_members():
    result = db.session.query(Member).all()
    response = list()
    for member in result:
        response.append({
            'id_no': member.id_no,
            'name': member.name,
            'major': member.major
        })

    return jsonify(response)


@app.route('/api/laptops')
def get_laptops():
    result = db.session.query(Laptop).all()
    response = list()
    for laptop in result:
        response.append({
            'serial_no': laptop.serial_no,
            'make': laptop.make,
            'owner': laptop.owner.id_no
        })

    return jsonify(response)

@login_manager.user_loader
def load_user(user_id):
    return Admin.query.filter(Admin.username.ilike(user_id)).first()
