import datetime
import os

ERROR_404_HELP = False
MONGODB_SETTINGS = {
    'db': 'dita_access',
    'alias': 'default',
    'host': 'localhost'
}
SECRET_KEY = 'Dita Admin'
SESSION_PROTECTION = "strong"
REMEMBER_COOKIE_DURATION = datetime.timedelta(days=7)
try:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL'].replace('postgres', 'postgresql')
except Exception as e:
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:root@localhost/dita_admin'
SQLALCHEMY_ECHO = True
