from api import db
from api.utils import get_current_time, get_current_date


class Member(db.Model):
    __tablename__ = 'members'
    id_no = db.Column(db.String(10), primary_key=True)
    name = db.Column(db.String(100))
    major = db.Column(db.String(4))
    image = db.Column(db.LargeBinary)
    laptops = db.relationship('Laptop', lazy='dynamic', cascade='delete,delete-orphan')

    def __init__(self, id_no, name, major):
        self.id_no = id_no
        self.name = name
        self.major = major

    def __eq__(self, other):
        return self.id_no == other.id_no

    def __repr__(self):
        return "%s - %s" % (self.id_no, self.name)

    def __str__(self):
        return "[%s] %s - %s" % (self.major, self.id_no, self.name)


class Laptop(db.Model):
    __tablename__ = 'laptops'

    serial_no = db.Column(db.String(50), unique=True, primary_key=True)
    make = db.Column(db.String(15))
    owner_id = db.Column(db.String(10), db.ForeignKey('members.id_no', ondelete='CASCADE'))
    owner = db.relationship('Member', back_populates='laptops', lazy='subquery')

    def __init__(self, serial, make, owner):
        self.serial_no = serial
        self.make = make
        self.owner = owner

    def __repr__(self):
        return "%s - %s" % (self.serial_no, self.make)


class Log(db.Model):
    __tablename__ = 'logs'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    member = db.relationship('Member')
    member_id = db.Column(db.String(4), db.ForeignKey(Member.id_no))
    date = db.Column(db.Date, default=get_current_date())
    time_in = db.Column(db.Time, default=get_current_time())
    time_out = db.Column(db.Time, default=None)

    def __init__(self, member):
        self.member = member

    @staticmethod
    def get_members_in():
        pass


class Admin(db.Model):
    __tablename__ = 'admins'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean, default=True)
    authenticated = db.Column(db.Boolean, default=True)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def __repr__(self):
        return "%s - %s" % (self.username, self.password)

    def is_active(self):
        return True

    def get_id(self):
        return self.username

    def is_authenticated(self):
        return self.authenticated

    def is_anonymous(self):
        return False


db.create_all()
